FROM golang

RUN go get \
    gitlab.com/colemanword/todo \
    github.com/gin-gonic/gin \
    github.com/jinzhu/gorm \
    github.com/jinzhu/gorm/dialects/mysql 

RUN  go run main.go